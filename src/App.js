import {useState} from "react";
import TodoInput from "./TodoInput";
import TodoList from "./TodoList";
import "./App.css";

const App = () => {
    const [todos, setTodos] = useState([]);
    const [todo, setTodo] = useState("");

    console.log(todo)

    const addTodo = () => {
        if (todo !== "") {
            setTodos([...todos, todo]);
            setTodo("");
        }
    };

    const deleteTodo = (todo) => {
        const newTodos = todos.filter((todo) => {
            return todo !== todo;
        });
    };


    const deleteTodoList = (text) => {
        const newTodos = todos.filter((todo) => {
            return todo !== text;
        });
    };

    return (
        <div className="App">
            <h1>React Todo App</h1>
            <TodoInput todo={todo} setTodo={setTodo} addTodo={addTodo}/>
            <TodoList list={todos} remove={deleteTodo}/>
        </div>
    );
};

export default App;
